/*
  edison_salesforce.ino
 
 Author:Steven Gall
 12-16-2014
 
 Edison board with temperature, light, sound sensors paired to a salesforce backend for data storage and reporting.
 
 */

#include <Wire.h>
#include <IoTkit.h>    // include IoTkit.h to use the Intel IoT Kit
#include <Ethernet.h>  // must be included to use IoTkit
#include <aJSON.h>
#include <stdio.h>
#include <rgb_lcd.h>

const int pinTemp = A0;      // pin of temperature sensor
const int pinLight = A1;     // pin of light sensor
const int pinSound = A2;     // pin of sound sensor
const int pinGas = A3;       // pin of gas sensor
const int pinLed = 5;        // pin of led module

const String temperatureSensorLabel = "temperature sensor";

rgb_lcd lcd;                 // lcd display
IoTkit iotkit;                // IoTkit for communication to Intel's dashboard
int B=3975;                  // B value of the thermistor


void setup()
{
  Serial.begin(115200);
  // Set up intel IoT Kit
  iotkit.begin();
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
}

void setLcdTextAutoscroll(String msg){
  // set the cursor to (0,0):
  lcd.setCursor(0, 0);
  lcd.print(msg);
  // scroll 16 positions (display length + string length) to the left
  // to move it back to center:
  for (int positionCounter = 0; positionCounter < msg.length(); positionCounter++) {
    // scroll one position left:
    lcd.scrollDisplayLeft(); 
    // wait a bit:
    delay(500);
  }
  // clear screen for the next loop:
  lcd.clear();
}
/*
------------------------------------
Sets text on the lcd screen.
------------------------------------
*/
void setLcdText(String row1,String row2){

  // clear screen for the next loop:
  lcd.clear();          

  // set the cursor to (0,0):
  lcd.setCursor(0, 0);
  // set the text of row 1
  lcd.print(row1);
  lcd.setCursor(0,1);
  // set the text of row 2
  lcd.print(row2);

}

/*
------------------------------------
Returns the temperature reading from the sensor in Fahrenheit
------------------------------------
*/
float getTemperature(){
  int temperatureReading = analogRead(pinTemp);                               // get analog value
  float resistance=(float)(1023-temperatureReading)*10000/temperatureReading; // get resistance
  float temperature=1/(log(resistance/10000)/B+1/298.15);                     // calculate temperature in Kelvin
  temperature -= 273;                                                         // convert Kelvin to Celsius
  temperature = (temperature * 9.0)/5.0+32.0;                                 // convert Celsius to Fahrenheit ...because America that's why! ;)
  return temperature;
}

/*
------------------------------------
Returns light reading from sensor
------------------------------------
*/
int getLight(){
  int lightReading = analogRead(pinLight);    //read the light sensor (get analog value)
  return lightReading;
}

/*
------------------------------------
Returns sound reading from sensor
------------------------------------
*/
int getSound(){
  int soundReading = analogRead(pinSound);   //read the sound sensor (get analog value)
  return soundReading;
}

String getSoundJson(){
  return "";
}

/*
------------------------------------
Returns gas reading from sensor
------------------------------------
*/
int getGas(){
  int gasReading = analogRead(pinGas);   //read the gas sensor (get analog value)
  return gasReading;
}
/*
------------------------------------
Callback from intel analytics
------------------------------------
*/
void callback(char* json) {
  Serial.println(json);
  aJsonObject* parsed = aJson.parse(json);
  if (&parsed == NULL) {
    // invalid or empty JSON
    Serial.println("recieved invalid JSON");
    return;
  }
  aJsonObject* component = aJson.getObjectItem(parsed, "component");
  aJsonObject* command = aJson.getObjectItem(parsed, "command");
  if ((component != NULL)) {
    if (strcmp(component->valuestring, "led") == 0) {
      if ((command != NULL)) {
        if (strcmp(command->valuestring, "off") == 0) {
          pinMode(5, OUTPUT);
          digitalWrite(5, false);
        }
        if (strcmp(command->valuestring, "on") == 0) {
          pinMode(5, OUTPUT);
          digitalWrite(5, true);
        }
      }
    }
  }
}
/*
------------------------------------
Sends the sound reading to intel analytics
------------------------------------
*/
void sendSoundIntel(){
  iotkit.send("Sound Sensor", getSound());
  // If you are receiving incoming commands, listen for them with receive
  // If you have your own custom json-parsing receive function, pass as argument
  // such as iotkit.receive(callback);
  // It must follow this prototype, but any name: void callback(char* json)
  //

  //iotkit.receive();
  iotkit.receive(callback);

  delay(1500);  
}
/*
------------------------------------
Sends the sound reading to salesforce
------------------------------------
*/
void sendSoundSalesforce(){
}

/*
------------------------------------
Sends the light reading to intel analytics
------------------------------------
*/
void sendLightIntel(){
  iotkit.send("Light Sensor", getLight());
  // If you are receiving incoming commands, listen for them with receive
  // If you have your own custom json-parsing receive function, pass as argument
  // such as iotkit.receive(callback);
  // It must follow this prototype, but any name: void callback(char* json)
  //

  //iotkit.receive();
  iotkit.receive(callback);

  delay(1500);  
}

/*
------------------------------------
Sends the light reading to salesforce
------------------------------------
*/
void sendLightSalesforce(){
}

/*
------------------------------------
Sends the gas reading to intel analytics
------------------------------------
*/
void sendGasIntel(){
  iotkit.send("Gas Sensor", getGas());
  // If you are receiving incoming commands, listen for them with receive
  // If you have your own custom json-parsing receive function, pass as argument
  // such as iotkit.receive(callback);
  // It must follow this prototype, but any name: void callback(char* json)
  //

  //iotkit.receive();
  iotkit.receive(callback);

  delay(1500);  
}

/*
------------------------------------
Sends the gas reading to salesforce
------------------------------------
*/
void sendGasSalesforce(){
}

/*
------------------------------------
Sends the temperature reading to intel analytics
------------------------------------
*/
void sendTemperatureIntel(){
  iotkit.send("Temperature Sensor", getTemperature());
  // If you are receiving incoming commands, listen for them with receive
  // If you have your own custom json-parsing receive function, pass as argument
  // such as iotkit.receive(callback);
  // It must follow this prototype, but any name: void callback(char* json)
  //

  //iotkit.receive();
  iotkit.receive(callback);

  delay(1500);  
}
/*
------------------------------------
Sends the temperature reading to salesforce
------------------------------------
*/
void sendTemperatureSalesforce(){
}

/*
------------------------------------
Sends all the readings to Intel anayltics
------------------------------------
*/
void sendAllIntel(){
  sendSoundIntel();
  sendLightIntel();
  sendGasIntel();
  sendTemperatureIntel();
}

/*
------------------------------------
Sends all the readings to salesforce
------------------------------------
*/
void sendAllSalesforce(){
  sendSoundSalesforce();
  sendLightSalesforce();
  sendGasSaleforce();
  sendTemperatureSalesforce();
}
String convertFloatToString(float num){
  char str[25];
  sprintf(str, "%f", num);
  return String(str);
}



/*
------------------------------------
Main loop
------------------------------------
*/
void loop()
{
  setLcdText("Temperature:", convertFloatToString(getTemperature()));
  delay(1000);
  setLcdText("Light Reading:", String(getLight()));
  delay(1000);
  setLcdText("Sound Reading:", String(getSound()));
  delay(1000);
  setLcdText("Gas Reading:", String(getGas()));
  delay(1000);
  //sendTemperatureIntel();

}

/*********************************************************************************************************
 * END FILE
 *********************************************************************************************************/

